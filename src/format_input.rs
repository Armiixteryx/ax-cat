use crate::program_state::*;

pub struct FormatInput<'a> {
    // The program options.
    program_state: &'a Options,

    // Line position of the writer.
    // It cannot be less than 1.
    line_position: u64,

    // To make sure that we reached a first valid textline.
    first_textline: bool,

    // Control when to print a newline.
    repeat_empty_line: u64,
}

impl<'a> FormatInput<'a> {
    pub fn new(actual_state: &Options) -> FormatInput {
        FormatInput {
            program_state: actual_state,
            line_position: 1u64,
            first_textline: false,
            repeat_empty_line: 0u64,
        }
    }

    pub fn run(&mut self, input: String) {
        if self.program_state.get_squeeze_blank() {
            self.squeeze_blank(&input);
        }

        if let LinesNumber::None = self.program_state.get_number() {
            //Nothing to do here.
        } else {
            self.show_numbers(&input);
        }

        if self.program_state.get_show_tabs() || self.program_state.get_show_nonprinting() {
            self.unicode_transformations(&input);
        } else {
            // Write input without transformations.
            print!("{}", input);
        }

        if self.repeat_empty_line < 2 {
            if self.program_state.get_show_ends() {
                print!("$");
            }

            // End of the line
            println!("");
        }
    }
}

impl<'a> FormatInput<'a> {
    fn squeeze_blank(&mut self, line: &str) {
        // For repeat_empty_line: Printing newline:
        // 0 => Line with text: Do.
        // 1 => First line without text: Do.
        // 2 => Second line without text and beyond: Do not.
        // Idk if it could be better done with enums...

        if line.is_empty() {
            self.repeat_empty_line += 1;
        } else {
            self.repeat_empty_line = 0;
        }
    }

    fn show_numbers(&mut self, line: &str) {
        fn calculate_space_behind(i: u64) -> &'static str {
            // This is the spaces behind the number line printing.
            let space_behind;

            if i > 0 && i < 10 {
                space_behind = "     ";
            } else if i >= 10 && i < 100 {
                space_behind = "    ";
            } else if i >= 100 && i < 1000 {
                space_behind = "   ";
            } else if i >= 1000 && i < 10000 {
                space_behind = "  ";
            } else if i >= 10000 && i < 100000 {
                space_behind = " ";
            } else {
                space_behind = "";
            }

            space_behind
        }

        let print_numbers = || {
            let space_behind = calculate_space_behind(self.line_position);
            print!("{}{}\t", space_behind, self.line_position);
        };

        match self.program_state.get_number() {
            LinesNumber::Blank => {
                if self.repeat_empty_line < 2 {
                    print_numbers();
                } else {
                    if self.line_position != 1 {
                        self.line_position -= 1;
                    }
                }

                self.line_position += 1;
            }

            LinesNumber::Nonblank => {
                if line.is_empty() {
                    if self.line_position != 1 {
                        self.line_position -= 1;
                    }
                } else {
                    print_numbers();
                    self.first_textline = true;
                }

                if self.first_textline {
                    self.line_position += 1;
                }
            }

            _ => (),
        }
    }

    fn unicode_transformations(&self, input: &str) {
        let mut new_input: Vec<u8> = Vec::new();

        for byte in input.as_bytes() {
            let mut buf_written = false;

            if self.program_state.get_show_tabs() {
                if *byte == 9u8 {
                    new_input.push(94u8);
                    new_input.push(73u8);
                    buf_written = true;
                }
            }

            if self.program_state.get_show_nonprinting() {
                unimplemented!();
            }

            if !buf_written {
                new_input.push(*byte);
            }
        }

        let new_input = String::from_utf8(new_input).unwrap();

        print!("{}", new_input);
    }
}
