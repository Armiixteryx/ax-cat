use std::env;
use std::fs;
use std::io::{self, BufRead, BufReader};
//use std::io::Read;
use std::process;

mod program_state;
use program_state::*;

mod format_input;
use format_input::*;

const fn help_msj() -> &'static str {
    "Usage: ax-cat [OPTION]... [FILE]...
Concatenate files to standard output like GNU cat does.

When no file is provided, or when file is \"-\", read from stdin.

  -b, --number-nonblank    number nonempty output lines, overrides -n
  -E, --show-ends          display $ at end of each line
  -n, --number             number all output lines
  -s, --squeeze-blank      suppress repeated empty output lines
  -T, --show-tabs          display TAB characters as ^I
      --help     display this help and exit

Notice:
Althougt I made this cat implementation to mimic the GNU cat output,
it is not guaranteed that it will generate outputs perfectly
equal to GNU cat.
"
}

fn read_stdin(formatter: &mut FormatInput) {
    loop {
        let mut input = String::new();

        io::stdin().read_line(&mut input).unwrap_or_else(|_| {
            eprintln!("ax-cat: I/O error while reading from stdin");
            process::exit(1);
        });

        formatter.run(input);
    }
}

fn main() {
    let mut options: Vec<String> = Vec::new();
    let mut files: Vec<String> = Vec::new();

    for (i, arg) in env::args().enumerate() {
        if i > 0 {
            if arg == "-" {
                files.push(arg);
            } else if arg.starts_with("-") {
                //TODO: Search a better way to make tokens
                options.push(arg);
            } else {
                files.push(arg);
            }
        }
    }

    let program_options = Options::new(options).unwrap_or_else(|err| {
        const ERR_MSJ1: &'static str = "ax-cat: unrecognized option: ";
        const ERR_MSJ2: &'static str = "Try 'ax-cat --help' for more information.";

        eprintln!("{}{}\n{}", ERR_MSJ1, err, ERR_MSJ2);
        process::exit(1);
    });

    if program_options.get_show_help() {
        const HELP_MSJ: &'static str = help_msj();

        println!("{}", HELP_MSJ);

        process::exit(1);
    }

    let mut formatter = FormatInput::new(&program_options);

    if files.is_empty() {
        //Read from stdin
        read_stdin(&mut formatter);
    } else {
        //Read from files
        for arg in files.iter() {
            if arg == "-" {
                read_stdin(&mut formatter);
            } else {
                let file = match fs::File::open(arg) {
                    Ok(file) => file,
                    Err(e) => match e.kind() {
                        io::ErrorKind::NotFound => {
                            eprintln!("ax-cat: {}: No such file or directory", arg);
                            process::exit(1);
                        }
                        io::ErrorKind::PermissionDenied => {
                            eprintln!("ax-cat: {}: Permission denied", arg);
                            process::exit(1);
                        }
                        _ => {
                            eprintln!("ax-cat: I/O error");
                            process::exit(1);
                        }
                    },
                };

                let reader = BufReader::new(file);

                for line in reader.lines() {
                    let input = line.unwrap_or_else(|_| {
                        eprintln!("ax-cat: Error while reading file");
                        process::exit(1);
                    });

                    formatter.run(input);
                }
            }
        }
    }
}
