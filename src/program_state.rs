#[derive(Debug)]
pub enum LinesNumber {
    Blank,    // -n, --number
    Nonblank, // -b, --number-nonblank
    None,
}

#[derive(Debug)]
pub struct Options {
    number: LinesNumber,
    show_ends: bool,        // -E, --show-ends
    squeeze_blank: bool,    // -s, --squeeze-blank
    show_tabs: bool,        // -T, --show-tabs
    show_nonprinting: bool, // -v, --show-nonprinting
    show_help: bool,
}

type Result<T> = std::result::Result<T, String>;

impl Options {
    pub fn new(options: Vec<String>) -> Result<Self> {
        let mut base_options = Options {
            number: LinesNumber::None,
            show_ends: false,
            squeeze_blank: false,
            show_tabs: false,
            show_nonprinting: false,
            show_help: false,
        };

        if !options.is_empty() {
            for option in options.into_iter() {
                // Show help
                if option == "--help" {
                    base_options.show_help = true;
                    break;
                }
                // LinesNumber nonblank
                else if option == "-b" || option == "--number-nonblank" {
                    base_options.number = LinesNumber::Nonblank;
                }
                // LinesNumber blank
                else if option == "-n" || option == "--number" {
                    match base_options.number {
                        LinesNumber::Nonblank => {} //-b overrides -n
                        _ => base_options.number = LinesNumber::Blank,
                    }
                }
                // Show ends
                else if option == "-E" || option == "--show-ends" {
                    base_options.show_ends = true;
                }
                // Squeeze blank
                else if option == "-s" || option == "--squeeze-blank" {
                    base_options.squeeze_blank = true;
                }
                // Show tabs
                else if option == "-T" || option == "--show-tabs" {
                    base_options.show_tabs = true;
                }
                // Show nonprinting
                else if option == "-v" || option == "--show-nonprinting" {
                    base_options.show_nonprinting = true;
                }
                else {
                    return Err(option);
                }
            }
        }

        Ok(base_options)
    }

    pub fn get_number(&self) -> &LinesNumber {
        &self.number
    }

    pub fn get_show_ends(&self) -> bool {
        self.show_ends
    }

    pub fn get_squeeze_blank(&self) -> bool {
        self.squeeze_blank
    }

    pub fn get_show_tabs(&self) -> bool {
        self.show_tabs
    }

    pub fn get_show_nonprinting(&self) -> bool {
        self.show_nonprinting
    }

    pub fn get_show_help(&self) -> bool {
        self.show_help
    }
}
