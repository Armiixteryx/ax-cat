
# ax-cat

*cat implementation in Rust.*

## What is ax-cat?

*ax-cat* means **A** rmiixtery **x** 's **cat**.

*ax-cat* does concatenation of files as in Unix-like systems *cat* program. Exactly, *ax-cat* tries to produce the same output as GNU Coreutils' implementation of *cat*, althought **not providing** the same performance.

It is a program made in the [Rust Programming Language](https://www.rust-lang.org/) for practice purpose.

> If you are searching for a really cool cat implementation, please look at [bat](https://github.com/sharkdp/bat) instead.

## See in action!
[![asciicast](https://asciinema.org/a/252292.svg)](https://asciinema.org/a/252292)

## Using ax-cat

### Usage
```
ax-cat [OPTION]... [FILE]...
```

Run `ax-cat --help` to see the options you may use.

## Building ax-cat

> I have not tested this program in macOS or Windows, just in Linux. It **should** work just fine in those platforms.

### Building from source using Cargo
 1. Install Rust. You may use [Rust guide](https://www.rust-lang.org/tools/install).
 2. Clone this repository.
 3. Build:
```bash
cargo build --release
```
 4. The binary should be located in `./target/release/ax-cat`. You may move it to a place where $PATH is configurated. Then try to run `ax-cat --help`. Or simply run the binary using `./ax-cat --help`.

### Running from source using Cargo

 1. Do the first two steps from the instructions.
 2. Build and run:
```bash
cargo run --release -- --help
```

## Why ax-cat?

 - To push into effect what I have learned in the awesome [book of Rust](https://doc.rust-lang.org/book/).
 - To learn how to implement those little programs I use on a daily basis.
 - To learn how to develop programs in general. This is my first serious program.

## Options already implemented
Using [this man page](https://linux.die.net/man/1/cat) as an options requeriments guide:

| Option | Status |
|--|--|
| `-A`, `--show-all` | ❎ |
| `-b`, `--number-nonblank` | ✅ |
| `-e` | ❎ |
| `-E`, `--show-ends` | ✅ |
| `-n`, `--number` | ✅ |
| `-s`, `--squeeze-blank` | ✅ |
| `-t` | ❎ |
| `-T`, `--show-tabs` | ✅ |
| `-v`, `--show-nonprinting` | 🆘 |
| `--help` | ✅ |
| `--version` | ❎ |

> Legend:
> ✅ = Implemented.
> ❎ = Not yet implemented.
> 🆘 = Help required.

## Actual limitations

* The actual representation of the data is made with Rust strings. Therefore, the file data **needs to be encoded in a valid UTF-8**. Otherwise the program will fail (in Rust slang, the program will **panic**). I am investigating how to overcome this limitation to allow *ax-cat* to read garbage from */dev/urandom*, for example.
* I am stuck in how to implement the `--show-nonprinting` option. I will appreciate any help on this.
* *ax-cat* is slower than GNU's *cat*. I think this is OK since this is a practice project only.
<!--stackedit_data:
eyJoaXN0b3J5IjpbNjc2MTE1Mjc0LDE2NjYxMDg2NjQsMTgxNz
QzNTg0NywtMTk4NTUwNTc1OF19
-->